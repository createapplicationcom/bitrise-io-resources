#!/bin/bash

# iOS application Resource folder path
if [ -z "$EXTRACT_TO_IOS_PATH" ]; then
  echo " [!] EXTRACT_TO_IOS_PATH is not provided - required!"
  exit 1
fi;

if [ ! -d "$EXTRACT_TO_IOS_PATH" ]; then
  echo " [!] $EXTRACT_TO_IOS_PATH doesn't exist"
  exit 1
fi;

# Android application Resource folder path
if [ -z "$EXTRACT_TO_ANDROID_PATH" ]; then
  echo " [!] EXTRACT_TO_ANDROID_PATH is not provided - required!"
  exit 1
fi;

if [ ! -d "$EXTRACT_TO_ANDROID_PATH" ]; then
  echo " [!] $EXTRACT_TO_ANDROID_PATH doesn't exist"
  exit 1
fi;


# Icons
if [ -z "$ICON1024X1024" ]; then
  echo " [!] icon 1024x1024 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON512X512" ]; then
  echo " [!] icon 512x512 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON300X300" ]; then
  echo " [!] icon 300x300 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON256X256" ]; then
  echo " [!] icon 256x256 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON196X196" ]; then
  echo " [!] icon 196x196 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON192X192" ]; then
  echo " [!] icon 192x192 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON180X180" ]; then
  echo " [!] icon 180x180 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON172X172" ]; then
  echo " [!] icon 172x172 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON167X167" ]; then
  echo " [!] icon 167x167 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON152X152" ]; then
  echo " [!] icon 152x152 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON144X144" ]; then
  echo " [!] icon 144x144 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON128X128" ]; then
  echo " [!] icon 128x128 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON120X120" ]; then
  echo " [!] icon 120x120 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON114X114" ]; then
  echo " [!] icon 114x114 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON100X100" ]; then
  echo " [!] icon 100x100 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON96X96" ]; then
  echo " [!] icon 96x96 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON88X88" ]; then
  echo " [!] icon 88x88 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON87X87" ]; then
  echo " [!] icon 87x87 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON80X80" ]; then
  echo " [!] icon 80x80 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON76X76" ]; then
  echo " [!] icon 76x76 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON72X72" ]; then
  echo " [!] icon 72x72 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON64X64" ]; then
  echo " [!] icon 64x64 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON58X58" ]; then
  echo " [!] icon 58x58 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON57X57" ]; then
  echo " [!] icon 57x57 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON55X55" ]; then
  echo " [!] icon 55x55 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON55X55" ]; then
  echo " [!] icon 55x55 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON50X50" ]; then
  echo " [!] icon 50x50 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON48X48" ]; then
  echo " [!] icon 48x48 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON40X40" ]; then
  echo " [!] icon 40x40 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON32X32" ]; then
  echo " [!] icon 32x32 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON29X29" ]; then
  echo " [!] icon 29x29 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON24X24" ]; then
  echo " [!] icon 24x24 is not provided - required!"
  exit 1
fi;

if [ -z "$ICON16X16" ]; then
  echo " [!] icon 16x16 is not provided - required!"
  exit 1
fi;


echo "------------------------------------------------"
echo " Inputs:"
echo "  EXTRACT_TO_IOS_PATH: $EXTRACT_TO_IOS_PATH"
echo "  EXTRACT_TO_ANDROID_PATH: $EXTRACT_TO_ANDROID_PATH"
echo ""
echo "  ICON1024X1024: $ICON1024X1024"
echo "  ICON512X512: $ICON512X512"
echo "  ICON300X300: $ICON300X300"
echo "  ICON256X256: $ICON256X256"
echo "  ICON196X196: $ICON196X196"
echo "  ICON192X192: $ICON192X192"
echo "  ICON180X180: $ICON180X180"
echo "  ICON172X172: $ICON172X172"
echo "  ICON167X167: $ICON167X167"
echo "  ICON152X152: $ICON152X152"
echo "  ICON144X144: $ICON144X144"
echo "  ICON128X128: $ICON128X128"
echo "  ICON120X120: $ICON120X120"
echo "  ICON114X114: $ICON114X114"
echo "  ICON100X100: $ICON100X100"
echo "  ICON96X96: $ICON96X96"
echo "  ICON88X88: $ICON88X88"
echo "  ICON87X87: $ICON87X87"
echo "  ICON80X80: $ICON80X80"
echo "  ICON76X76: $ICON76X76"
echo "  ICON72X72: $ICON72X72"
echo "  ICON64X64: $ICON64X64"
echo "  ICON58X58: $ICON58X58"
echo "  ICON57X57: $ICON57X57"
echo "  ICON55X55: $ICON55X55"
echo "  ICON50X50: $ICON50X50"
echo "  ICON48X48: $ICON48X48"
echo "  ICON40X40: $ICON40X40"
echo "  ICON32X32: $ICON32X32"
echo "  ICON29X29: $ICON29X29"
echo "  ICON24X24: $ICON24X24"
echo "  ICON16X16: $ICON16X16"
echo "------------------------------------------------"

function download
{
  curl -fo "downloads/$2" $1
  curl_result=$?
  if [ $curl_result -eq 0 ]; then
    echo " (i) OK - $2"
  else
    echo " [!] Download failed - $1 (error code: $curl_result)"
    exit $curl_result
  fi;
}

function replace_ios
{
	if [ ! -f "downloads/$1" ]; then
	  echo " [!] $1 doesn't exist"
	  exit 1
	fi;

	cp "downloads/$1" "$EXTRACT_TO_IOS_PATH/$2"
	echo "downloads/$1 > $EXTRACT_TO_IOS_PATH/$2"
}

function replace_android
{
	if [ ! -f "downloads/$1" ]; then
	  echo " [!] $1 doesn't exist"
	  exit 1
	fi;

	cp "downloads/$1" "$EXTRACT_TO_ANDROID_PATH/$2"
	echo "downloads/$1 > $EXTRACT_TO_ANDROID_PATH/$2"
}

# --- Preparations
mkdir "downloads"

# download $ICON1024X1024 "icon1024x1024.png"
# download $ICON512X512 "icon512x512.png"
# download $ICON300X300 "icon300x300.png"
# download $ICON256X256 "icon256x256.png"
# download $ICON196X196 "icon196x196.png"
# download $ICON192X192 "icon192x192.png"

download $ICON180X180 "icon180x180.png"
replace_ios "icon180x180.png" "icon-60@3x.png"

# download $ICON172X172 "icon172x172.png"
# download $ICON167X167 "icon167x167.png"

download $ICON152X152 "icon152x152.png"
replace_ios "icon152x152.png" "icon-76@2x.png"

download $ICON144X144 "icon144x144.png"
replace_ios "icon144x144.png" "icon-72@2x.png"

download $ICON128X128 "icon128x128.png"
replace_android "icon128x128.png" "drawable/Icon.png"

download $ICON120X120 "icon120x120.png"
replace_ios "icon120x120.png" "icon-60@2x.png"

download $ICON114X114 "icon114x114.png"
replace_ios "icon114x114.png" "icon@2x.png"

download $ICON100X100 "icon100x100.png"
replace_ios "icon100x100.png" "icon-50@2x.png"

# download $ICON96X96 "icon96x96.png"
# download $ICON88X88 "icon88x88.png"
# download $ICON87X87 "icon87x87.png"

download $ICON80X80 "icon80x80.png"
replace_ios "icon80x80.png" "icon-40@2x.png"

download $ICON76X76 "icon76x76.png"
replace_ios "icon76x76.png" "icon-76.png"

download $ICON72X72 "icon72x72.png"
replace_ios "icon72x72.png" "icon-72.png"

# download $ICON64X64 "icon64x64.png"

download $ICON58X58 "icon58x58.png"
replace_ios "icon58x58.png" "icon-small@2x.png"

download $ICON57X57 "icon57x57.png"
replace_ios "icon57x57.png" "icon.png"

# download $ICON55X55 "icon55x55.png"

download $ICON50X50 "icon50x50.png"
replace_ios "icon50x50.png" "icon-50.png"

# download $ICON48X48 "icon48x48.png"

download $ICON40X40 "icon40x40.png"
replace_ios "icon40x40.png" "icon-40.png"

# download $ICON32X32 "icon32x32.png"

download $ICON29X29 "icon29x29.png"
replace_ios "icon29x29.png" "icon-small.png"

# download $ICON24X24 "icon24x24.png"
# download $ICON16X16 "icon16x16.png"

exit 0